﻿namespace SimCorp
{
    class Program
    {
        static void Main(string[] args)
        {
            string? fileName = "";

            PrintToConsole("Please type the file name (should be in current folder):");
            fileName = Console.ReadLine();

            string text = "";
            if (fileName != null)
            {

                var path = Path.Combine(Directory.GetCurrentDirectory(), fileName);

                try
                {
                    text = File.ReadAllText(path);
                }
                catch (Exception e)
                {
                    if (e.GetType() == typeof(FileNotFoundException))
                    {
                        PrintToConsole("file not found, try again later");
                        return;
                    }

                    PrintToConsole(e.Message);
                    throw;
                }
            }

            void PrintToConsole(string? test)
            {
                Console.WriteLine(test);
            }

            var dictionary = Occurrence(text);

            foreach (var kvp in dictionary)
            {
                Console.WriteLine("{1}: {0}", kvp.Key, kvp.Value);
            }
        }

        public static Dictionary<string, int> Occurrence(string text)
        {
            var dictionary = new Dictionary<string, int>();

            if (text.Replace(" ", "").Length == 0)
            {
                return dictionary;
            }

            foreach (var word in text.Split(" "))
            {
                if (dictionary.ContainsKey(word.ToLower()))
                {
                    dictionary[word] = dictionary.GetValueOrDefault(word) + 1;
                }
                else
                {
                    dictionary.Add(word.ToLower(), 1);
                }
            }

            return dictionary;
        }
    }
}