# Count word occurrence in file
## Description
Service helps to count word's occurrences in present file

## Technical details

- [Source code](https://gitlab.com/kovalexeyko/simcorpc) in GitLab.

## PREREQUISITES

* setup .NET core
* prepare file with words separated by space

## How to run

There are few ways to run the program:
* run from IDE Program.cs

## Result
Expected result for string "Go do that thing that you do so well" will be:
```
1: go
2: do
2: that
1: thing
1: you
1: so
1: well
```
